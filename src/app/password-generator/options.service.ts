import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OptionsService {

  // Default options
  options = {
    length    : 8,
    minLength : 5,
    maxLength : 32,
    withPunct       : true,
    noProgramSyntax : false,
    noSimilar       : false,
    showOptions     : true
  };

  constructor() { }

  getOptions(){
    if (localStorage.getItem('options') === null) {
      return this.options;
    } else {
      let optionsJSON = localStorage.getItem('options');
      return JSON.parse(optionsJSON);
    }
  }
  saveOptions(options){
    localStorage.setItem('options', JSON.stringify(options))
  }
  deleteOptions(){
    localStorage.removeItem('options');
  }
}
