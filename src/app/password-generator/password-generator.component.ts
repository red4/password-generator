import { Component, OnInit, NgModule } from '@angular/core';
import { OptionsService } from './options.service';

@Component({
  selector: 'app-password-generator',
  templateUrl: './password-generator.component.html',
  styleUrls: ['./password-generator.component.scss'],
  providers: [OptionsService]
})
export class PasswordGeneratorComponent implements OnInit {
  
  options = this.optionsService.getOptions();
  password:string = '';
  
  constructor(private optionsService:OptionsService) { }
  
  ngOnInit() {
  }

  private getCharacters(withPunctuations, noProgramSyntax, noSimilar){
    let characters = '';
    let charactersDefault = {
      punctuations: '!-_:;',
      numbers: '23456789',
      letters: 'ABCDEFGHJKLMNPQRSTUVWXYZabcdefghjkmnpqrstuvwxyz',
      programming: '$&?@#<>(){}[]"',
      similarLetters: '1iIlL0oO'
    }
    characters = charactersDefault.letters;
    if(withPunctuations){
      characters += charactersDefault.punctuations;
    }
    if(!noProgramSyntax){
      characters += charactersDefault.programming;
    }
    if(!noSimilar){
      characters += charactersDefault.similarLetters;
    }
    return characters;
  }
  
  onGenerate(){
    let pwdChars:string = this.getCharacters(this.options.withPunct, this.options.noProgramSyntax, this.options.noSimilar);
    this.password = Array(this.options.length).fill(pwdChars).map(function(x) { return x[Math.floor(Math.random() * x.length)] }).join('');
  }

  onUpdateOptions(){
    if(this.options.length >= this.options.minLength || this.options.length <= this.options.maxLength){
      this.onGenerate();
    } else {
      console.log('Wrong variables');
    }
  }

  onToggleOptions() {
    if(this.options.showOptions){
      this.options.showOptions = false;
    } else {
      this.options.showOptions = true;
    }
  }

  onSaveOptions(){
    this.optionsService.saveOptions(this.options);
  }

  onCopyToClipboard(elem:HTMLInputElement){
    elem.select();
    document.execCommand('copy');
    elem.setSelectionRange(0, 0);    
  }
}
